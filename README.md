*** Exam project in Data Analytics: Coffeebar Simulation ***


      WHAT IS THE PROGRAM MADE FOR?

The program simulates a coffee bar that sells drinks and foods.  

There are 4 types of customers :  -  the returning regular 
			    -  the returning hipster
			    -  the one time regular 
			    -  the one time tripadvisor 


      HOW TO USE THE PROGRAM?

Simply import the file named CFsimulation.py.  Import random and numpy (as np).  
Then, use the function named general_function.
This function has 3 parameters : days (number of days of the simulation), regular (number of regular customers) 
and hipster (number of hipster customers).

==>  general_function(days, regular, hipster)


      HOW DOES THE PROGRAM WORK?

When general_function is called, first, the regular and hipster customers will be created and stocked.  Then, for each day and for each time 
in the list of the different time, a customer will be choser randomly (it can be either a tripadvisor one, a regular one time one, a regular one
or a hipster one).  Then, a drink and sometimes a food will also be selected randomly.  The budget of the customer will be reduced because 
he is going to buy these stuffs.  If the customer doesn't have enough money to buy the most expensive drink and the most expensive food, he
will no longer be able to be chosen from the list.  At the end, the program will return a dictionary of all the customers and their information.  


      AUTHORS 

Roman Mathieu & Joachim Daisomont 
As part of the data analytics exam

