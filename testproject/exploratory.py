import csv
import matplotlib.pyplot as plt

# Open file

with open('Coffeebar_2013-2017.csv', newline='') as csvfile:
    myfile = csv.reader(csvfile, delimiter=';', quotechar=' ')

# Create lists of the items sold, of the time periods and of the different id's of the customers

    drinkList = []
    foodList = []
    timeList = []
    idList = []

# Initialize all the number of foods and drinks to 0

    nbsandwich = 0; nbcookie = 0; nbpie = 0; nbmilkshake = 0; nbfrappucino = 0; nbwater = 0; nbtea = 0
    nbcoffee = 0; nbsoda = 0; nbmuffin = 0

    for row in myfile:

        # Complete drinkList

        if row[2] not in drinkList and row[2] != "DRINKS":
            drinkList.append(row[2])
        if row[2] == "milkshake":
            nbmilkshake += 1
        if row[2] == "frappucino":
            nbfrappucino += 1
        if row[2] == "water":
            nbwater += 1
        if row[2] == "tea":
            nbtea += 1
        if row[2] == "coffee":
            nbcoffee += 1
        if row[2] == "soda":
            nbsoda += 1

        # Complete foodList

        if row[3] not in foodList and row[3] != "FOOD":
            foodList.append(row[3])
        if row[3] == "sandwich":
            nbsandwich += 1
        if row[3] == "cookie":
            nbcookie += 1
        if row[3] == "pie":
            nbpie += 1
        if row[3] == "muffin":
            nbmuffin += 1

        # Complete idList

#        if row[1] not in idList:
#           idList.append(row[1])

        # Complete timeList

        if row[0][-8:] not in timeList and len(row[0][-8:]) == 8:
            timeList.append(row[0][-8:])

    # Create dictionary of the price and the number of items sold

    fooddictionary = {"sandwich": nbsandwich, "cookie": nbcookie, "pie": nbpie, "muffin": nbmuffin}
    pricefooddictionary = {"": 0, "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3}
    pricedrinkdictionary = {"frappucino": 4, "soda": 3, "tea": 3, "water": 2, "milkshake": 5, "coffee": 3}
    drinkdictionary = {"frappucino": nbfrappucino, "soda": nbsoda, "tea": nbtea, "water": nbwater,
                       "milkshake": nbmilkshake, "coffee": nbcoffee}


# Create lists of income of each item (for the additional graph)

    foodIncomeList = []
    drinkIncomeList = []

    for food in fooddictionary:
        for price in pricefooddictionary:
            if food == price:
                income = fooddictionary[food]*pricefooddictionary[price]
                foodIncomeList.append(income)

    for drink in drinkdictionary:
        for price in pricedrinkdictionary:
            if drink == price:
                income = drinkdictionary[drink]*pricedrinkdictionary[price]
                drinkIncomeList.append(income)

    # Graph for the amount of foods sold

    plt.bar(list(fooddictionary.keys()), fooddictionary.values(), color='g')
    plt.title("Amount of foods sold ")
    plt.ylabel("Number of foods sold")
    plt.xlabel("Kind of foods")
    plt.savefig("FoodsSoldDataBase.png")
    plt.show()

    # Graph for the amount of drinks sold

    plt.bar(list(drinkdictionary.keys()), drinkdictionary.values(), color='r')
    plt.title("Amount of drinks sold")
    plt.ylabel("Number of drinks sold")
    plt.xlabel("Kind of drinks")
    plt.savefig("DrinksSoldDataBase.png")
    plt.show()

    # Pie graph of the turnover per product (interesting additional graph)

    # Data to plot
    labels = 'Sandwich', 'Cookie', 'Pie', 'Muffin', 'Frappucino', 'Soda', 'Tea', 'Water', 'Milkshake', 'Coffee'
    sizes = [foodIncomeList[0], foodIncomeList[1], foodIncomeList[2], foodIncomeList[3], drinkIncomeList[0],
             drinkIncomeList[1], drinkIncomeList[2], drinkIncomeList[3], drinkIncomeList[4], drinkIncomeList[5]]

    colors = ("red", "green", "orange", "cyan", "brown", "grey", "blue", "indigo", "beige", "yellow")
    explode = (0.1, 0, 0, 0, 0, 0, 0, 0, 0, 0)  # explode 1st slice

    # Plot
    plt.pie(sizes, explode=explode, labels=labels, colors=colors,
            autopct='%1.1f%%', shadow=True, startangle=140)
    plt.title("Turnover per product")
    plt.axis('equal')
    plt.show()

    # Create dictionary of probabilities for each time

    probaPerTime = {}

    # Create list of foods and drinks sold for each time + dictionary of the number of type of foods and drinks sold

    for hour in timeList:
        probaPerTime[hour] = []
        foodPerTime = []
        drinkPerTime = []
        countfood = {}
        countdrink = {}

        with open('Coffeebar_2013-2017.csv', newline='') as csvfile:
            myfile = csv.reader(csvfile, delimiter=';', quotechar=' ')

            # Complete list of foods and drinks by time

            for row in myfile:
                if row[0][-8:] == hour:
                    foodPerTime.append(row[3])
                    drinkPerTime.append(row[2])

            # Count number of foods and drinks in the lists foodPerTime and drinkPerTime

            for drink in drinkList:
                x = drinkPerTime.count(drink)
                countdrink[drink] = x

            for food in foodList:
                x = foodPerTime.count(food)
                countfood[food] = x

            # Count the total number of drinks and food sold

            sumfood = 0
            sumdrink = 0

            for nbfood in countfood.values():
                sumfood += nbfood

            for nbdrink in countdrink.values():
                sumdrink += nbdrink

            # Compute the probabilities of the food and drinks sold

            for food, nbfood in countfood.items():
                proba = (nbfood/sumfood)*100
                listPre = probaPerTime.get(hour)
                listPre.append([food, round(proba, 3)])
                probaPerTime[hour] = listPre

            for drink, nbdrink in countdrink.items():
                y = (nbdrink/sumdrink)*100
                listPre = probaPerTime.get(hour)
                listPre.append([drink, round(y, 2)])
                probaPerTime[hour] = listPre

    print('Answer to the questions part 1 : ')
    print('___________________________________________________________________________________________________')
    print('                                                                                                   ')
    print('What food and drinks are sold by the coffee bar? How many unique customers did the bar have?')
    print('                                                                                                   ')
    print('The bar sells : ')
    print(*drinkList, sep=", ")
    print('                                                                                                   ')
    print('The bar sells : ')
    print(*foodList[1:], sep=", ")
    print('                                                                                                  ')
    print('There are 247 989 unique customers in the data base')
    # The number of customers could be found with the commands at the line 55 and 56 but it takes a long time.
    # When we ran it, we obtained 247 989 unique customers.

    print('                                          ')
    for hour in timeList:
        listProba = []
        for prob in probaPerTime[hour]:
            listProba.append(prob[1])
        print('On average the probability of a customer at ' + hour + ' buying nothing as food, sandwich, pie, muffin, '
              'cookie, frappucino, soda, coffee, tea, water or milkshake is ' + str(listProba))
        print('                           ')
