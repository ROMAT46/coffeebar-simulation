import random
import numpy as np
import matplotlib.pyplot as plt


def general_function(days, regular, hipster):
    itera = 1
    suminc = 0
    nbInStockFood = {'sandwich' : 100000, 'pie': 30000, 'muffin' : 10000, 'cookie' : 2500}
    nbInStockDrink = {'milkshake' :125000, 'water' : 32555, 'tea' : 150000, 'coffee' :  200000, 'soda' : 30000,
                      'frappucino' : 27852}
    foodlist = ['', 'sandwich', 'pie', 'muffin', 'cookie']
    drinklist = ['milkshake', 'water', 'tea', 'coffee', 'soda', 'frappucino']
    newdrinkdict = {"frappucino": 0, "soda": 0, "tea": 0, "water": 0, "milkshake": 0, "coffee": 0}
    newfooddict = {"sandwich": 0, "cookie": 0, "pie": 0, "muffin": 0}
    probapertime = {'08:00:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.47], ['soda', 12.82], ['coffee', 31.56], ['tea', 12.66],
                                 ['water', 14.03], ['milkshake', 14.47]],
                    '08:05:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.77], ['soda', 12.99], ['coffee', 34.63], ['tea', 12.88],
                                 ['water', 12.66], ['milkshake', 14.08]],
                    '08:10:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.88], ['soda', 13.42], ['coffee', 32.38], ['tea', 13.53],
                                 ['water', 13.86], ['milkshake', 13.92]],
                    '08:15:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.15], ['soda', 13.97], ['coffee', 32.38], ['tea', 14.47],
                                 ['water', 13.26], ['milkshake', 12.77]],
                    '08:20:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.81], ['soda', 13.1], ['coffee', 33.81], ['tea', 13.48],
                                 ['water', 12.93], ['milkshake', 12.88]],
                    '08:25:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.32], ['soda', 12.99], ['coffee', 33.1], ['tea', 13.21],
                                 ['water', 14.03], ['milkshake', 13.37]],
                    '08:30:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.97], ['soda', 12.77], ['coffee', 33.37], ['tea', 11.89],
                                 ['water', 14.58], ['milkshake', 13.42]],
                    '08:35:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.58], ['soda', 13.92], ['coffee', 31.01], ['tea', 13.04],
                                 ['water', 14.03], ['milkshake', 13.42]],
                    '08:40:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.42], ['soda', 14.79], ['coffee', 34.08], ['tea', 13.42],
                                 ['water', 12.71], ['milkshake', 11.56]],
                    '08:45:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.52], ['soda', 12.33], ['coffee', 31.62], ['tea', 13.1],
                                 ['water', 13.97], ['milkshake', 14.47]],
                    '08:50:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.04], ['soda', 14.47], ['coffee', 32.44], ['tea', 13.15],
                                 ['water', 13.15], ['milkshake', 13.75]],
                    '08:55:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.93], ['soda', 14.41], ['coffee', 32.22], ['tea', 13.26],
                                 ['water', 13.97], ['milkshake', 13.21]],
                    '09:00:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.88], ['soda', 14.19], ['coffee', 33.81], ['tea', 14.47],
                                 ['water', 12.16], ['milkshake', 12.49]],
                    '09:05:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.66], ['soda', 14.9], ['coffee', 31.34], ['tea', 13.26],
                                 ['water', 14.36], ['milkshake', 13.48]],
                    '09:10:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 11.84], ['soda', 12.49], ['coffee', 34.03], ['tea', 13.32],
                                 ['water', 13.37], ['milkshake', 14.96]],
                    '09:15:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.42], ['soda', 14.3], ['coffee', 32.22], ['tea', 13.15],
                                 ['water', 13.64], ['milkshake', 13.26]],
                    '09:20:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 11.78], ['soda', 14.25], ['coffee', 33.37], ['tea', 13.21],
                                 ['water', 13.64], ['milkshake', 13.75]],
                    '09:25:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.15], ['soda', 12.38], ['coffee', 34.85], ['tea', 13.48],
                                 ['water', 13.48], ['milkshake', 12.66]],
                    '09:30:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.38], ['soda', 12.93], ['coffee', 34.79], ['tea', 12.82],
                                 ['water', 13.04], ['milkshake', 14.03]],
                    '09:35:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 11.78], ['soda', 14.03], ['coffee', 33.04], ['tea', 14.52],
                                 ['water', 13.15], ['milkshake', 13.48]],
                    '09:40:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.52], ['soda', 13.32], ['coffee', 33.1], ['tea', 11.78],
                                 ['water', 13.86], ['milkshake', 13.42]],
                    '09:45:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.74], ['soda', 14.25], ['coffee', 33.26], ['tea', 12.6],
                                 ['water', 12.66], ['milkshake', 12.49]],
                    '09:50:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.97], ['soda', 12.93], ['coffee', 32.93], ['tea', 13.1],
                                 ['water', 13.48], ['milkshake', 13.59]],
                    '09:55:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.81], ['soda', 14.25], ['coffee', 30.63], ['tea', 12.88],
                                 ['water', 13.53], ['milkshake', 14.9]],
                    '10:00:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.49], ['soda', 13.81], ['coffee', 33.48], ['tea', 12.88],
                                 ['water', 13.7], ['milkshake', 13.64]],
                    '10:05:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.21], ['soda', 13.59], ['coffee', 31.4], ['tea', 13.97],
                                 ['water', 13.64], ['milkshake', 14.19]],
                    '10:10:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.3], ['soda', 14.68], ['coffee', 32.05], ['tea', 12.49],
                                 ['water', 13.37], ['milkshake', 13.1]],
                    '10:15:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.1], ['soda', 14.08], ['coffee', 32.88], ['tea', 14.47],
                                 ['water', 12.77], ['milkshake', 12.71]],
                    '10:20:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.7], ['soda', 14.68], ['coffee', 32.6], ['tea', 12.99],
                                 ['water', 13.92], ['milkshake', 12.11]],
                    '10:25:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.55], ['soda', 14.08], ['coffee', 32.38], ['tea', 14.58],
                                 ['water', 12.93], ['milkshake', 13.48]],
                    '10:30:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.97], ['soda', 13.48], ['coffee', 32.38], ['tea', 13.04],
                                 ['water', 14.19], ['milkshake', 12.93]],
                    '10:35:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.3], ['soda', 13.48], ['coffee', 32.93], ['tea', 14.3],
                                 ['water', 12.66], ['milkshake', 12.33]],
                    '10:40:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.82], ['soda', 13.75], ['coffee', 34.85], ['tea', 11.62],
                                 ['water', 14.58], ['milkshake', 12.38]],
                    '10:45:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 12.99], ['soda', 14.03], ['coffee', 31.45], ['tea', 13.37],
                                 ['water', 13.53], ['milkshake', 14.63]],
                    '10:50:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 14.19], ['soda', 13.04], ['coffee', 33.21], ['tea', 12.77],
                                 ['water', 13.42], ['milkshake', 13.37]],
                    '10:55:00': [['', 100.0], ['sandwich', 0.0], ['pie', 0.0], ['muffin', 0.0], ['cookie', 0.0],
                                 ['frappucino', 13.7], ['soda', 14.85], ['coffee', 32.77], ['tea', 13.59],
                                 ['water', 12.77], ['milkshake', 12.33]],
                    '11:00:00': [['', 0.0], ['sandwich', 61.534], ['pie', 12.0], ['muffin', 13.808], ['cookie', 12.658],
                                 ['frappucino', 7.34], ['soda', 57.64], ['coffee', 8.38], ['tea', 8.44],
                                 ['water', 8.11], ['milkshake', 10.08]],
                    '11:02:00': [['', 0.0], ['sandwich', 63.726], ['pie', 13.041], ['muffin', 12.438],
                                 ['cookie', 10.795], ['frappucino', 7.4], ['soda', 58.08], ['coffee', 8.6],
                                 ['tea', 9.37], ['water', 8.27], ['milkshake', 8.27]],
                    '11:04:00': [['', 0.0], ['sandwich', 62.575], ['pie', 13.315], ['muffin', 12.438],
                                 ['cookie', 11.671], ['frappucino', 8.93], ['soda', 57.32], ['coffee', 7.89],
                                 ['tea', 7.78], ['water', 9.92], ['milkshake', 8.16]],
                    '11:06:00': [['', 0.0], ['sandwich', 62.192], ['pie', 11.562], ['muffin', 13.589],
                                 ['cookie', 12.658], ['frappucino', 8.6], ['soda', 58.03], ['coffee', 7.84],
                                 ['tea', 9.32], ['water', 8.38], ['milkshake', 7.84]],
                    '11:08:00': [['', 0.0], ['sandwich', 61.973], ['pie', 13.26], ['muffin', 12.164],
                                 ['cookie', 12.603], ['frappucino', 8.99], ['soda', 58.03], ['coffee', 9.15],
                                 ['tea', 7.45], ['water', 7.51], ['milkshake', 8.88]],
                    '11:10:00': [['', 0.0], ['sandwich', 64.219], ['pie', 12.712], ['muffin', 11.233],
                                 ['cookie', 11.836], ['frappucino', 8.16], ['soda', 58.25], ['coffee', 8.16],
                                 ['tea', 8.27], ['water', 9.42], ['milkshake', 7.73]],
                    '11:12:00': [['', 0.0], ['sandwich', 63.288], ['pie', 12.438], ['muffin', 11.616],
                                 ['cookie', 12.658], ['frappucino', 7.89], ['soda', 57.92], ['coffee', 10.25],
                                 ['tea', 7.18], ['water', 8.33], ['milkshake', 8.44]],
                    '11:14:00': [['', 0.0], ['sandwich', 61.151], ['pie', 12.767], ['muffin', 13.205],
                                 ['cookie', 12.877], ['frappucino', 9.7], ['soda', 56.44], ['coffee', 7.84],
                                 ['tea', 8.38], ['water', 8.33], ['milkshake', 9.32]],
                    '11:16:00': [['', 0.0], ['sandwich', 63.616], ['pie', 12.164], ['muffin', 12.0], ['cookie', 12.219],
                                 ['frappucino', 7.95], ['soda', 60.66], ['coffee', 8.11], ['tea', 8.27],
                                 ['water', 7.51], ['milkshake', 7.51]],
                    '11:18:00': [['', 0.0], ['sandwich', 63.123], ['pie', 12.932], ['muffin', 10.904],
                                 ['cookie', 13.041], ['frappucino', 8.16], ['soda', 57.42], ['coffee', 7.78],
                                 ['tea', 8.93], ['water', 8.27], ['milkshake', 9.42]],
                    '11:20:00': [['', 0.0], ['sandwich', 60.712], ['pie', 13.479], ['muffin', 13.534],
                                 ['cookie', 12.274], ['frappucino', 8.05], ['soda', 57.32], ['coffee', 8.66],
                                 ['tea', 8.6], ['water', 8.27], ['milkshake', 9.1]],
                    '11:22:00': [['', 0.0], ['sandwich', 62.356], ['pie', 13.26], ['muffin', 11.178],
                                 ['cookie', 13.205], ['frappucino', 8.16], ['soda', 58.03], ['coffee', 8.88],
                                 ['tea', 8.22], ['water', 8.11], ['milkshake', 8.6]],
                    '11:24:00': [['', 0.0], ['sandwich', 62.849], ['pie', 11.562], ['muffin', 13.26],
                                 ['cookie', 12.329], ['frappucino', 9.48], ['soda', 58.25], ['coffee', 8.05],
                                 ['tea', 7.51], ['water', 8.6], ['milkshake', 8.11]],
                    '11:26:00': [['', 0.0], ['sandwich', 61.315], ['pie', 13.151], ['muffin', 13.096],
                                 ['cookie', 12.438], ['frappucino', 8.6], ['soda', 57.1], ['coffee', 7.95],
                                 ['tea', 7.23], ['water', 8.88], ['milkshake', 10.25]],
                    '11:28:00': [['', 0.0], ['sandwich', 62.959], ['pie', 11.89], ['muffin', 13.425],
                                 ['cookie', 11.726], ['frappucino', 7.18], ['soda', 58.9], ['coffee', 9.48],
                                 ['tea', 7.23], ['water', 8.66], ['milkshake', 8.55]],
                    '11:30:00': [['', 0.0], ['sandwich', 63.452], ['pie', 12.877], ['muffin', 12.274],
                                 ['cookie', 11.397], ['frappucino', 6.79], ['soda', 59.29], ['coffee', 8.27],
                                 ['tea', 8.49], ['water', 8.6], ['milkshake', 8.55]],
                    '11:32:00': [['', 0.0], ['sandwich', 63.945], ['pie', 12.603], ['muffin', 10.63],
                                 ['cookie', 12.822], ['frappucino', 8.88], ['soda', 59.01], ['coffee', 8.93],
                                 ['tea', 7.78], ['water', 8.27], ['milkshake', 7.12]],
                    '11:34:00': [['', 0.0], ['sandwich', 62.904], ['pie', 12.164], ['muffin', 13.041],
                                 ['cookie', 11.89], ['frappucino', 8.71], ['soda', 58.47], ['coffee', 8.44],
                                 ['tea', 8.16], ['water', 7.89], ['milkshake', 8.33]],
                    '11:36:00': [['', 0.0], ['sandwich', 64.164], ['pie', 12.274], ['muffin', 12.274],
                                 ['cookie', 11.288], ['frappucino', 7.67], ['soda', 61.21], ['coffee', 7.62],
                                 ['tea', 6.96], ['water', 8.49], ['milkshake', 8.05]],
                    '11:38:00': [['', 0.0], ['sandwich', 63.507], ['pie', 12.438], ['muffin', 11.288],
                                 ['cookie', 12.767], ['frappucino', 7.84], ['soda', 59.51], ['coffee', 8.38],
                                 ['tea', 7.4], ['water', 8.33], ['milkshake', 8.55]],
                    '11:40:00': [['', 0.0], ['sandwich', 63.178], ['pie', 12.822], ['muffin', 11.89], ['cookie', 12.11],
                                 ['frappucino', 8.27], ['soda', 58.3], ['coffee', 8.55], ['tea', 9.15], ['water', 7.62],
                                 ['milkshake', 8.11]],
                    '11:42:00': [['', 0.0], ['sandwich', 63.836], ['pie', 13.151], ['muffin', 11.945],
                                 ['cookie', 11.068], ['frappucino', 8.22], ['soda', 59.73], ['coffee', 7.78],
                                 ['tea', 7.95], ['water', 8.11], ['milkshake', 8.22]],
                    '11:44:00': [['', 0.0], ['sandwich', 61.315], ['pie', 11.452], ['muffin', 12.767],
                                 ['cookie', 14.466], ['frappucino', 7.45], ['soda', 56.99], ['coffee', 9.04],
                                 ['tea', 8.11], ['water', 9.7], ['milkshake', 8.71]],
                    '11:46:00': [['', 0.0], ['sandwich', 63.945], ['pie', 10.959], ['muffin', 12.658],
                                 ['cookie', 12.438], ['frappucino', 6.68], ['soda', 59.73], ['coffee', 7.78],
                                 ['tea', 8.66], ['water', 7.67], ['milkshake', 9.48]],
                    '11:48:00': [['', 0.0], ['sandwich', 61.151], ['pie', 12.0], ['muffin', 14.027], ['cookie', 12.822],
                                 ['frappucino', 8.71], ['soda', 56.33], ['coffee', 9.7], ['tea', 8.6], ['water', 8.93],
                                 ['milkshake', 7.73]],
                    '11:50:00': [['', 0.0], ['sandwich', 61.699], ['pie', 13.37], ['muffin', 13.096],
                                 ['cookie', 11.836], ['frappucino', 8.44], ['soda', 58.79], ['coffee', 7.45],
                                 ['tea', 8.82], ['water', 8.44], ['milkshake', 8.05]],
                    '11:52:00': [['', 0.0], ['sandwich', 63.562], ['pie', 12.329], ['muffin', 12.493],
                                 ['cookie', 11.616], ['frappucino', 7.84], ['soda', 58.79], ['coffee', 8.82],
                                 ['tea', 8.27], ['water', 8.44], ['milkshake', 7.84]],
                    '11:54:00': [['', 0.0], ['sandwich', 64.384], ['pie', 11.014], ['muffin', 13.315],
                                 ['cookie', 11.288], ['frappucino', 8.55], ['soda', 60.44], ['coffee', 8.0],
                                 ['tea', 6.47], ['water', 7.18], ['milkshake', 9.37]],
                    '11:56:00': [['', 0.0], ['sandwich', 64.274], ['pie', 12.164], ['muffin', 11.178],
                                 ['cookie', 12.384], ['frappucino', 8.33], ['soda', 59.78], ['coffee', 7.73],
                                 ['tea', 7.67], ['water', 8.38], ['milkshake', 8.11]],
                    '11:58:00': [['', 0.0], ['sandwich', 62.849], ['pie', 13.37], ['muffin', 11.726],
                                 ['cookie', 12.055], ['frappucino', 7.51], ['soda', 59.01], ['coffee', 8.71],
                                 ['tea', 7.51], ['water', 8.66], ['milkshake', 8.6]],
                    '12:00:00': [['', 0.0], ['sandwich', 64.493], ['pie', 12.493], ['muffin', 10.356],
                                 ['cookie', 12.658], ['frappucino', 7.84], ['soda', 58.36], ['coffee', 8.71],
                                 ['tea', 8.71], ['water', 8.22], ['milkshake', 8.16]],
                    '12:02:00': [['', 0.0], ['sandwich', 61.863], ['pie', 12.877], ['muffin', 11.945],
                                 ['cookie', 13.315], ['frappucino', 8.44], ['soda', 57.15], ['coffee', 7.67],
                                 ['tea', 9.1], ['water', 8.66], ['milkshake', 8.99]],
                    '12:04:00': [['', 0.0], ['sandwich', 63.945], ['pie', 11.945], ['muffin', 12.329],
                                 ['cookie', 11.781], ['frappucino', 8.82], ['soda', 57.81], ['coffee', 8.33],
                                 ['tea', 8.16], ['water', 8.44], ['milkshake', 8.44]],
                    '12:06:00': [['', 0.0], ['sandwich', 62.849], ['pie', 11.562], ['muffin', 11.89],
                                 ['cookie', 13.699], ['frappucino', 8.22], ['soda', 58.58], ['coffee', 8.82],
                                 ['tea', 8.71], ['water', 7.78], ['milkshake', 7.89]],
                    '12:08:00': [['', 0.0], ['sandwich', 62.904], ['pie', 11.945], ['muffin', 12.877],
                                 ['cookie', 12.274], ['frappucino', 9.26], ['soda', 58.25], ['coffee', 8.0],
                                 ['tea', 8.27], ['water', 8.22], ['milkshake', 8.0]],
                    '12:10:00': [['', 0.0], ['sandwich', 61.918], ['pie', 12.986], ['muffin', 12.548],
                                 ['cookie', 12.548], ['frappucino', 7.34], ['soda', 57.04], ['coffee', 8.33],
                                 ['tea', 9.1], ['water', 8.82], ['milkshake', 9.37]],
                    '12:12:00': [['', 0.0], ['sandwich', 62.411], ['pie', 13.37], ['muffin', 12.219], ['cookie', 12.0],
                                 ['frappucino', 7.95], ['soda', 57.97], ['coffee', 8.33], ['tea', 8.99],
                                 ['water', 8.55], ['milkshake', 8.22]],
                    '12:14:00': [['', 0.0], ['sandwich', 64.164], ['pie', 11.123], ['muffin', 12.767],
                                 ['cookie', 11.945], ['frappucino', 8.33], ['soda', 59.4], ['coffee', 7.56],
                                 ['tea', 7.62], ['water', 9.15], ['milkshake', 7.95]],
                    '12:16:00': [['', 0.0], ['sandwich', 61.26], ['pie', 12.822], ['muffin', 12.548], ['cookie', 13.37],
                                 ['frappucino', 8.93], ['soda', 56.49], ['coffee', 7.78], ['tea', 8.38],
                                 ['water', 9.53], ['milkshake', 8.88]],
                    '12:18:00': [['', 0.0], ['sandwich', 61.589], ['pie', 13.315], ['muffin', 12.712],
                                 ['cookie', 12.384], ['frappucino', 7.34], ['soda', 58.19], ['coffee', 9.21],
                                 ['tea', 8.27], ['water', 7.95], ['milkshake', 9.04]],
                    '12:20:00': [['', 0.0], ['sandwich', 63.068], ['pie', 11.452], ['muffin', 13.096],
                                 ['cookie', 12.384], ['frappucino', 7.89], ['soda', 58.85], ['coffee', 9.37],
                                 ['tea', 7.51], ['water', 7.78], ['milkshake', 8.6]],
                    '12:22:00': [['', 0.0], ['sandwich', 62.356], ['pie', 12.384], ['muffin', 13.205],
                                 ['cookie', 12.055], ['frappucino', 7.18], ['soda', 58.96], ['coffee', 9.04],
                                 ['tea', 8.55], ['water', 7.45], ['milkshake', 8.82]],
                    '12:24:00': [['', 0.0], ['sandwich', 62.575], ['pie', 12.055], ['muffin', 12.438],
                                 ['cookie', 12.932], ['frappucino', 8.22], ['soda', 57.81], ['coffee', 8.11],
                                 ['tea', 8.22], ['water', 8.44], ['milkshake', 9.21]],
                    '12:26:00': [['', 0.0], ['sandwich', 60.0], ['pie', 13.315], ['muffin', 13.315], ['cookie', 13.37],
                                 ['frappucino', 9.32], ['soda', 55.84], ['coffee', 8.33], ['tea', 8.44], ['water', 9.7],
                                 ['milkshake', 8.38]],
                    '12:28:00': [['', 0.0], ['sandwich', 63.781], ['pie', 12.219], ['muffin', 12.493],
                                 ['cookie', 11.507], ['frappucino', 8.0], ['soda', 58.79], ['coffee', 8.44],
                                 ['tea', 7.12], ['water', 8.93], ['milkshake', 8.71]],
                    '12:30:00': [['', 0.0], ['sandwich', 62.137], ['pie', 12.877], ['muffin', 12.712],
                                 ['cookie', 12.274], ['frappucino', 9.7], ['soda', 58.47], ['coffee', 7.78],
                                 ['tea', 8.55], ['water', 7.01], ['milkshake', 8.49]],
                    '12:32:00': [['', 0.0], ['sandwich', 63.507], ['pie', 12.055], ['muffin', 11.671],
                                 ['cookie', 12.767], ['frappucino', 8.33], ['soda', 58.68], ['coffee', 9.04],
                                 ['tea', 8.71], ['water', 7.34], ['milkshake', 7.89]],
                    '12:34:00': [['', 0.0], ['sandwich', 60.219], ['pie', 12.274], ['muffin', 12.932],
                                 ['cookie', 14.575], ['frappucino', 8.49], ['soda', 56.16], ['coffee', 8.55],
                                 ['tea', 9.21], ['water', 8.33], ['milkshake', 9.26]],
                    '12:36:00': [['', 0.0], ['sandwich', 61.973], ['pie', 13.041], ['muffin', 12.274],
                                 ['cookie', 12.712], ['frappucino', 8.22], ['soda', 58.14], ['coffee', 8.33],
                                 ['tea', 7.95], ['water', 9.04], ['milkshake', 8.33]],
                    '12:38:00': [['', 0.0], ['sandwich', 62.466], ['pie', 12.986], ['muffin', 12.055],
                                 ['cookie', 12.493], ['frappucino', 7.84], ['soda', 58.85], ['coffee', 7.78],
                                 ['tea', 9.64], ['water', 7.12], ['milkshake', 8.77]],
                    '12:40:00': [['', 0.0], ['sandwich', 64.493], ['pie', 12.822], ['muffin', 11.233],
                                 ['cookie', 11.452], ['frappucino', 8.44], ['soda', 60.0], ['coffee', 8.6],
                                 ['tea', 8.33], ['water', 6.74], ['milkshake', 7.89]],
                    '12:42:00': [['', 0.0], ['sandwich', 62.466], ['pie', 12.055], ['muffin', 13.315],
                                 ['cookie', 12.164], ['frappucino', 9.15], ['soda', 59.73], ['coffee', 7.18],
                                 ['tea', 8.27], ['water', 8.82], ['milkshake', 6.85]],
                    '12:44:00': [['', 0.0], ['sandwich', 61.753], ['pie', 12.767], ['muffin', 12.0], ['cookie', 13.479],
                                 ['frappucino', 7.78], ['soda', 59.84], ['coffee', 8.11], ['tea', 8.11],
                                 ['water', 8.33], ['milkshake', 7.84]],
                    '12:46:00': [['', 0.0], ['sandwich', 63.014], ['pie', 12.767], ['muffin', 12.384],
                                 ['cookie', 11.836], ['frappucino', 9.21], ['soda', 59.62], ['coffee', 9.04],
                                 ['tea', 8.27], ['water', 6.74], ['milkshake', 7.12]],
                    '12:48:00': [['', 0.0], ['sandwich', 61.205], ['pie', 12.932], ['muffin', 13.315],
                                 ['cookie', 12.548], ['frappucino', 9.15], ['soda', 56.38], ['coffee', 8.71],
                                 ['tea', 8.88], ['water', 8.71], ['milkshake', 8.16]],
                    '12:50:00': [['', 0.0], ['sandwich', 63.068], ['pie', 12.822], ['muffin', 12.219],
                                 ['cookie', 11.89], ['frappucino', 8.88], ['soda', 58.3], ['coffee', 7.84],
                                 ['tea', 8.11], ['water', 8.49], ['milkshake', 8.38]],
                    '12:52:00': [['', 0.0], ['sandwich', 60.932], ['pie', 12.986], ['muffin', 12.384],
                                 ['cookie', 13.699], ['frappucino', 7.84], ['soda', 57.1], ['coffee', 9.15],
                                 ['tea', 9.42], ['water', 8.82], ['milkshake', 7.67]],
                    '12:54:00': [['', 0.0], ['sandwich', 61.808], ['pie', 11.616], ['muffin', 12.877],
                                 ['cookie', 13.699], ['frappucino', 8.0], ['soda', 59.29], ['coffee', 8.11],
                                 ['tea', 8.66], ['water', 8.0], ['milkshake', 7.95]],
                    '12:56:00': [['', 0.0], ['sandwich', 63.726], ['pie', 11.836], ['muffin', 11.616],
                                 ['cookie', 12.822], ['frappucino', 8.05], ['soda', 59.07], ['coffee', 8.6],
                                 ['tea', 8.11], ['water', 7.45], ['milkshake', 8.71]],
                    '12:58:00': [['', 0.0], ['sandwich', 62.466], ['pie', 12.164], ['muffin', 11.342],
                                 ['cookie', 14.027], ['frappucino', 8.27], ['soda', 57.86], ['coffee', 8.38],
                                 ['tea', 9.21], ['water', 8.22], ['milkshake', 8.05]],
                    '13:00:00': [['', 60.548], ['sandwich', 0.0], ['pie', 12.877], ['muffin', 12.822],
                                 ['cookie', 13.753], ['frappucino', 16.16], ['soda', 16.16], ['coffee', 17.81],
                                 ['tea', 16.99], ['water', 14.79], ['milkshake', 18.08]],
                    '13:04:00': [['', 59.781], ['sandwich', 0.0], ['pie', 13.699], ['muffin', 13.26], ['cookie', 13.26],
                                 ['frappucino', 17.53], ['soda', 17.21], ['coffee', 14.63], ['tea', 18.03],
                                 ['water', 16.6], ['milkshake', 16.0]],
                    '13:08:00': [['', 60.438], ['sandwich', 0.0], ['pie', 13.589], ['muffin', 13.425],
                                 ['cookie', 12.548], ['frappucino', 16.05], ['soda', 15.29], ['coffee', 16.88],
                                 ['tea', 17.1], ['water', 18.41], ['milkshake', 16.27]],
                    '13:12:00': [['', 60.767], ['sandwich', 0.0], ['pie', 14.247], ['muffin', 12.329],
                                 ['cookie', 12.658], ['frappucino', 16.0], ['soda', 16.88], ['coffee', 18.52],
                                 ['tea', 16.22], ['water', 16.55], ['milkshake', 15.84]],
                    '13:16:00': [['', 58.466], ['sandwich', 0.0], ['pie', 14.521], ['muffin', 13.589],
                                 ['cookie', 13.425], ['frappucino', 17.1], ['soda', 13.92], ['coffee', 16.27],
                                 ['tea', 16.71], ['water', 17.53], ['milkshake', 18.47]],
                    '13:20:00': [['', 58.192], ['sandwich', 0.0], ['pie', 14.904], ['muffin', 13.479],
                                 ['cookie', 13.425], ['frappucino', 14.58], ['soda', 18.52], ['coffee', 17.92],
                                 ['tea', 15.95], ['water', 16.33], ['milkshake', 16.71]],
                    '13:24:00': [['', 60.877], ['sandwich', 0.0], ['pie', 12.603], ['muffin', 12.603],
                                 ['cookie', 13.918], ['frappucino', 16.93], ['soda', 16.6], ['coffee', 15.95],
                                 ['tea', 16.99], ['water', 17.59], ['milkshake', 15.95]],
                    '13:28:00': [['', 58.904], ['sandwich', 0.0], ['pie', 14.301], ['muffin', 13.37],
                                 ['cookie', 13.425], ['frappucino', 16.99], ['soda', 17.86], ['coffee', 16.33],
                                 ['tea', 15.62], ['water', 16.49], ['milkshake', 16.71]],
                    '13:32:00': [['', 57.918], ['sandwich', 0.0], ['pie', 12.164], ['muffin', 15.507],
                                 ['cookie', 14.411], ['frappucino', 16.05], ['soda', 18.08], ['coffee', 15.12],
                                 ['tea', 17.81], ['water', 16.22], ['milkshake', 16.71]],
                    '13:36:00': [['', 59.397], ['sandwich', 0.0], ['pie', 14.521], ['muffin', 13.151],
                                 ['cookie', 12.932], ['frappucino', 17.59], ['soda', 17.75], ['coffee', 17.32],
                                 ['tea', 16.05], ['water', 16.77], ['milkshake', 14.52]],
                    '13:40:00': [['', 58.521], ['sandwich', 0.0], ['pie', 14.082], ['muffin', 13.534],
                                 ['cookie', 13.863], ['frappucino', 15.73], ['soda', 16.93], ['coffee', 16.44],
                                 ['tea', 18.3], ['water', 16.0], ['milkshake', 16.6]],
                    '13:44:00': [['', 61.37], ['sandwich', 0.0], ['pie', 12.438], ['muffin', 13.37], ['cookie', 12.822],
                                 ['frappucino', 16.71], ['soda', 16.82], ['coffee', 15.73], ['tea', 17.04],
                                 ['water', 18.08], ['milkshake', 15.62]],
                    '13:48:00': [['', 61.425], ['sandwich', 0.0], ['pie', 12.767], ['muffin', 12.11],
                                 ['cookie', 13.699], ['frappucino', 15.56], ['soda', 16.55], ['coffee', 16.49],
                                 ['tea', 16.16], ['water', 17.7], ['milkshake', 17.53]],
                    '13:52:00': [['', 59.178], ['sandwich', 0.0], ['pie', 14.192], ['muffin', 12.767],
                                 ['cookie', 13.863], ['frappucino', 16.05], ['soda', 17.1], ['coffee', 17.26],
                                 ['tea', 16.27], ['water', 16.44], ['milkshake', 16.88]],
                    '13:56:00': [['', 59.836], ['sandwich', 0.0], ['pie', 12.767], ['muffin', 13.425],
                                 ['cookie', 13.973], ['frappucino', 17.75], ['soda', 17.92], ['coffee', 16.49],
                                 ['tea', 15.67], ['water', 15.67], ['milkshake', 16.49]],
                    '14:00:00': [['', 60.548], ['sandwich', 0.0], ['pie', 13.151], ['muffin', 13.041],
                                 ['cookie', 13.26], ['frappucino', 16.0], ['soda', 16.88], ['coffee', 17.53],
                                 ['tea', 17.64], ['water', 16.99], ['milkshake', 14.96]],
                    '14:04:00': [['', 59.781], ['sandwich', 0.0], ['pie', 12.164], ['muffin', 14.521],
                                 ['cookie', 13.534], ['frappucino', 15.34], ['soda', 18.58], ['coffee', 16.66],
                                 ['tea', 16.49], ['water', 16.93], ['milkshake', 16.0]],
                    '14:08:00': [['', 60.932], ['sandwich', 0.0], ['pie', 14.027], ['muffin', 11.836],
                                 ['cookie', 13.205], ['frappucino', 17.42], ['soda', 15.67], ['coffee', 15.18],
                                 ['tea', 17.64], ['water', 16.82], ['milkshake', 17.26]],
                    '14:12:00': [['', 58.904], ['sandwich', 0.0], ['pie', 13.205], ['muffin', 14.137],
                                 ['cookie', 13.753], ['frappucino', 16.16], ['soda', 17.04], ['coffee', 16.44],
                                 ['tea', 17.37], ['water', 16.77], ['milkshake', 16.22]],
                    '14:16:00': [['', 59.397], ['sandwich', 0.0], ['pie', 14.795], ['muffin', 12.11],
                                 ['cookie', 13.699], ['frappucino', 17.04], ['soda', 15.56], ['coffee', 16.55],
                                 ['tea', 17.32], ['water', 16.16], ['milkshake', 17.37]],
                    '14:20:00': [['', 59.397], ['sandwich', 0.0], ['pie', 15.014], ['muffin', 12.877],
                                 ['cookie', 12.712], ['frappucino', 15.84], ['soda', 16.22], ['coffee', 17.7],
                                 ['tea', 16.44], ['water', 15.73], ['milkshake', 18.08]],
                    '14:24:00': [['', 61.918], ['sandwich', 0.0], ['pie', 13.644], ['muffin', 13.096],
                                 ['cookie', 11.342], ['frappucino', 16.55], ['soda', 16.6], ['coffee', 17.21],
                                 ['tea', 15.23], ['water', 16.88], ['milkshake', 17.53]],
                    '14:28:00': [['', 60.986], ['sandwich', 0.0], ['pie', 12.658], ['muffin', 13.425],
                                 ['cookie', 12.932], ['frappucino', 16.66], ['soda', 16.99], ['coffee', 16.88],
                                 ['tea', 16.33], ['water', 17.32], ['milkshake', 15.84]],
                    '14:32:00': [['', 60.438], ['sandwich', 0.0], ['pie', 13.479], ['muffin', 14.082], ['cookie', 12.0],
                                 ['frappucino', 17.04], ['soda', 15.51], ['coffee', 16.33], ['tea', 16.33],
                                 ['water', 17.37], ['milkshake', 17.42]],
                    '14:36:00': [['', 61.096], ['sandwich', 0.0], ['pie', 12.548], ['muffin', 12.712],
                                 ['cookie', 13.644], ['frappucino', 15.34], ['soda', 17.1], ['coffee', 16.71],
                                 ['tea', 17.26], ['water', 17.86], ['milkshake', 15.73]],
                    '14:40:00': [['', 60.877], ['sandwich', 0.0], ['pie', 13.479], ['muffin', 13.315],
                                 ['cookie', 12.329], ['frappucino', 16.66], ['soda', 15.95], ['coffee', 17.1],
                                 ['tea', 16.49], ['water', 17.7], ['milkshake', 16.11]],
                    '14:44:00': [['', 59.89], ['sandwich', 0.0], ['pie', 13.041], ['muffin', 14.247],
                                 ['cookie', 12.822], ['frappucino', 17.15], ['soda', 14.52], ['coffee', 18.19],
                                 ['tea', 17.37], ['water', 16.44], ['milkshake', 16.33]],
                    '14:48:00': [['', 59.014], ['sandwich', 0.0], ['pie', 13.479], ['muffin', 14.521],
                                 ['cookie', 12.986], ['frappucino', 18.03], ['soda', 16.0], ['coffee', 15.23],
                                 ['tea', 16.22], ['water', 17.1], ['milkshake', 17.42]],
                    '14:52:00': [['', 61.753], ['sandwich', 0.0], ['pie', 13.644], ['muffin', 10.959],
                                 ['cookie', 13.644], ['frappucino', 16.33], ['soda', 15.67], ['coffee', 17.75],
                                 ['tea', 16.6], ['water', 15.4], ['milkshake', 18.25]],
                    '14:56:00': [['', 59.836], ['sandwich', 0.0], ['pie', 12.712], ['muffin', 13.479],
                                 ['cookie', 13.973], ['frappucino', 15.4], ['soda', 17.04], ['coffee', 16.33],
                                 ['tea', 14.52], ['water', 19.4], ['milkshake', 17.32]],
                    '15:00:00': [['', 60.986], ['sandwich', 0.0], ['pie', 13.37], ['muffin', 13.425],
                                 ['cookie', 12.219], ['frappucino', 14.79], ['soda', 17.26], ['coffee', 17.75],
                                 ['tea', 16.27], ['water', 17.42], ['milkshake', 16.49]],
                    '15:04:00': [['', 60.986], ['sandwich', 0.0], ['pie', 13.315], ['muffin', 12.932],
                                 ['cookie', 12.767], ['frappucino', 16.11], ['soda', 18.41], ['coffee', 16.16],
                                 ['tea', 15.62], ['water', 16.93], ['milkshake', 16.77]],
                    '15:08:00': [['', 61.644], ['sandwich', 0.0], ['pie', 12.986], ['muffin', 11.726],
                                 ['cookie', 13.644], ['frappucino', 16.82], ['soda', 16.44], ['coffee', 18.52],
                                 ['tea', 16.66], ['water', 16.16], ['milkshake', 15.4]],
                    '15:12:00': [['', 59.507], ['sandwich', 0.0], ['pie', 13.753], ['muffin', 13.26],
                                 ['cookie', 13.479], ['frappucino', 16.93], ['soda', 16.22], ['coffee', 16.22],
                                 ['tea', 16.82], ['water', 16.44], ['milkshake', 17.37]],
                    '15:16:00': [['', 59.781], ['sandwich', 0.0], ['pie', 13.808], ['muffin', 13.096],
                                 ['cookie', 13.315], ['frappucino', 16.49], ['soda', 16.33], ['coffee', 17.97],
                                 ['tea', 15.89], ['water', 16.33], ['milkshake', 16.99]],
                    '15:20:00': [['', 57.863], ['sandwich', 0.0], ['pie', 13.479], ['muffin', 14.466],
                                 ['cookie', 14.192], ['frappucino', 16.6], ['soda', 16.77], ['coffee', 17.86],
                                 ['tea', 15.62], ['water', 14.79], ['milkshake', 18.36]],
                    '15:24:00': [['', 59.671], ['sandwich', 0.0], ['pie', 13.096], ['muffin', 13.37],
                                 ['cookie', 13.863], ['frappucino', 17.48], ['soda', 16.88], ['coffee', 17.64],
                                 ['tea', 15.18], ['water', 16.93], ['milkshake', 15.89]],
                    '15:28:00': [['', 59.671], ['sandwich', 0.0], ['pie', 14.137], ['muffin', 12.712],
                                 ['cookie', 13.479], ['frappucino', 16.99], ['soda', 15.12], ['coffee', 18.96],
                                 ['tea', 17.53], ['water', 16.22], ['milkshake', 15.18]],
                    '15:32:00': [['', 60.11], ['sandwich', 0.0], ['pie', 13.589], ['muffin', 14.247],
                                 ['cookie', 12.055], ['frappucino', 15.78], ['soda', 16.71], ['coffee', 16.16],
                                 ['tea', 16.99], ['water', 17.64], ['milkshake', 16.71]],
                    '15:36:00': [['', 60.822], ['sandwich', 0.0], ['pie', 12.877], ['muffin', 13.205],
                                 ['cookie', 13.096], ['frappucino', 18.47], ['soda', 16.93], ['coffee', 16.88],
                                 ['tea', 16.44], ['water', 16.33], ['milkshake', 14.96]],
                    '15:40:00': [['', 59.726], ['sandwich', 0.0], ['pie', 13.151], ['muffin', 12.384],
                                 ['cookie', 14.74], ['frappucino', 16.71], ['soda', 16.22], ['coffee', 17.86],
                                 ['tea', 17.37], ['water', 16.6], ['milkshake', 15.23]],
                    '15:44:00': [['', 59.178], ['sandwich', 0.0], ['pie', 14.356], ['muffin', 13.479],
                                 ['cookie', 12.986], ['frappucino', 16.66], ['soda', 16.16], ['coffee', 17.15],
                                 ['tea', 15.29], ['water', 18.58], ['milkshake', 16.16]],
                    '15:48:00': [['', 61.644], ['sandwich', 0.0], ['pie', 12.384], ['muffin', 13.37],
                                 ['cookie', 12.603], ['frappucino', 17.21], ['soda', 16.44], ['coffee', 16.77],
                                 ['tea', 15.95], ['water', 16.16], ['milkshake', 17.48]],
                    '15:52:00': [['', 57.863], ['sandwich', 0.0], ['pie', 13.315], ['muffin', 15.123],
                                 ['cookie', 13.699], ['frappucino', 15.12], ['soda', 16.22], ['coffee', 16.44],
                                 ['tea', 16.77], ['water', 18.79], ['milkshake', 16.66]],
                    '15:56:00': [['', 59.123], ['sandwich', 0.0], ['pie', 14.301], ['muffin', 14.356],
                                 ['cookie', 12.219], ['frappucino', 16.99], ['soda', 18.52], ['coffee', 17.15],
                                 ['tea', 15.73], ['water', 16.11], ['milkshake', 15.51]],
                    '16:00:00': [['', 62.192], ['sandwich', 0.0], ['pie', 12.219], ['muffin', 12.603],
                                 ['cookie', 12.986], ['frappucino', 17.04], ['soda', 16.6], ['coffee', 15.89],
                                 ['tea', 17.32], ['water', 17.15], ['milkshake', 16.0]],
                    '16:04:00': [['', 61.26], ['sandwich', 0.0], ['pie', 12.438], ['muffin', 13.26], ['cookie', 13.041],
                                 ['frappucino', 16.55], ['soda', 16.6], ['coffee', 16.71], ['tea', 16.55],
                                 ['water', 16.93], ['milkshake', 16.66]],
                    '16:08:00': [['', 60.274], ['sandwich', 0.0], ['pie', 13.534], ['muffin', 13.973],
                                 ['cookie', 12.219], ['frappucino', 15.84], ['soda', 16.6], ['coffee', 17.42],
                                 ['tea', 16.0], ['water', 17.7], ['milkshake', 16.44]],
                    '16:12:00': [['', 60.767], ['sandwich', 0.0], ['pie', 13.534], ['muffin', 13.096],
                                 ['cookie', 12.603], ['frappucino', 17.59], ['soda', 17.21], ['coffee', 17.48],
                                 ['tea', 16.05], ['water', 15.67], ['milkshake', 16.0]],
                    '16:16:00': [['', 59.288], ['sandwich', 0.0], ['pie', 13.753], ['muffin', 13.205],
                                 ['cookie', 13.753], ['frappucino', 16.11], ['soda', 15.56], ['coffee', 17.75],
                                 ['tea', 16.49], ['water', 15.67], ['milkshake', 18.41]],
                    '16:20:00': [['', 59.671], ['sandwich', 0.0], ['pie', 13.37], ['muffin', 14.027],
                                 ['cookie', 12.932], ['frappucino', 17.21], ['soda', 17.04], ['coffee', 14.47],
                                 ['tea', 17.48], ['water', 17.04], ['milkshake', 16.77]],
                    '16:24:00': [['', 58.685], ['sandwich', 0.0], ['pie', 13.863], ['muffin', 13.096],
                                 ['cookie', 14.356], ['frappucino', 15.89], ['soda', 16.77], ['coffee', 18.52],
                                 ['tea', 16.0], ['water', 16.99], ['milkshake', 15.84]],
                    '16:28:00': [['', 59.014], ['sandwich', 0.0], ['pie', 14.685], ['muffin', 12.767],
                                 ['cookie', 13.534], ['frappucino', 15.51], ['soda', 15.51], ['coffee', 18.19],
                                 ['tea', 17.81], ['water', 16.55], ['milkshake', 16.44]],
                    '16:32:00': [['', 59.671], ['sandwich', 0.0], ['pie', 13.644], ['muffin', 13.37],
                                 ['cookie', 13.315], ['frappucino', 15.95], ['soda', 17.37], ['coffee', 17.26],
                                 ['tea', 17.26], ['water', 16.44], ['milkshake', 15.73]],
                    '16:36:00': [['', 60.932], ['sandwich', 0.0], ['pie', 13.151], ['muffin', 12.877],
                                 ['cookie', 13.041], ['frappucino', 17.64], ['soda', 14.52], ['coffee', 16.22],
                                 ['tea', 17.75], ['water', 17.97], ['milkshake', 15.89]],
                    '16:40:00': [['', 58.192], ['sandwich', 0.0], ['pie', 13.753], ['muffin', 14.849],
                                 ['cookie', 13.205], ['frappucino', 16.66], ['soda', 16.16], ['coffee', 17.04],
                                 ['tea', 17.04], ['water', 16.0], ['milkshake', 17.1]],
                    '16:44:00': [['', 60.384], ['sandwich', 0.0], ['pie', 12.877], ['muffin', 13.205],
                                 ['cookie', 13.534], ['frappucino', 16.05], ['soda', 16.33], ['coffee', 15.62],
                                 ['tea', 16.49], ['water', 18.58], ['milkshake', 16.93]],
                    '16:48:00': [['', 61.151], ['sandwich', 0.0], ['pie', 13.425], ['muffin', 12.877],
                                 ['cookie', 12.548], ['frappucino', 13.97], ['soda', 15.73], ['coffee', 16.49],
                                 ['tea', 17.97], ['water', 17.04], ['milkshake', 18.79]],
                    '16:52:00': [['', 58.411], ['sandwich', 0.0], ['pie', 13.918], ['muffin', 13.315],
                                 ['cookie', 14.356], ['frappucino', 16.38], ['soda', 16.66], ['coffee', 16.55],
                                 ['tea', 16.55], ['water', 18.14], ['milkshake', 15.73]],
                    '16:56:00': [['', 60.438], ['sandwich', 0.0], ['pie', 12.877], ['muffin', 12.712],
                                 ['cookie', 13.973], ['frappucino', 16.71], ['soda', 16.38], ['coffee', 17.92],
                                 ['tea', 16.0], ['water', 16.11], ['milkshake', 16.88]],
                    '17:00:00': [['', 59.616], ['sandwich', 0.0], ['pie', 14.082], ['muffin', 13.534],
                                 ['cookie', 12.767], ['frappucino', 15.45], ['soda', 16.93], ['coffee', 18.25],
                                 ['tea', 16.77], ['water', 17.32], ['milkshake', 15.29]],
                    '17:04:00': [['', 59.123], ['sandwich', 0.0], ['pie', 13.534], ['muffin', 12.603],
                                 ['cookie', 14.74], ['frappucino', 16.05], ['soda', 15.18], ['coffee', 16.99],
                                 ['tea', 16.49], ['water', 18.47], ['milkshake', 16.82]],
                    '17:08:00': [['', 60.219], ['sandwich', 0.0], ['pie', 12.164], ['muffin', 13.644],
                                 ['cookie', 13.973], ['frappucino', 16.6], ['soda', 16.55], ['coffee', 15.12],
                                 ['tea', 16.77], ['water', 17.42], ['milkshake', 17.53]],
                    '17:12:00': [['', 61.205], ['sandwich', 0.0], ['pie', 13.534], ['muffin', 12.055],
                                 ['cookie', 13.205], ['frappucino', 16.6], ['soda', 15.34], ['coffee', 17.59],
                                 ['tea', 16.99], ['water', 16.71], ['milkshake', 16.77]],
                    '17:16:00': [['', 60.986], ['sandwich', 0.0], ['pie', 12.986], ['muffin', 12.767],
                                 ['cookie', 13.26], ['frappucino', 16.49], ['soda', 17.7], ['coffee', 14.9],
                                 ['tea', 17.59], ['water', 16.77], ['milkshake', 16.55]],
                    '17:20:00': [['', 59.836], ['sandwich', 0.0], ['pie', 12.822], ['muffin', 13.205],
                                 ['cookie', 14.137], ['frappucino', 17.37], ['soda', 15.34], ['coffee', 17.1],
                                 ['tea', 17.97], ['water', 15.45], ['milkshake', 16.77]],
                    '17:24:00': [['', 60.877], ['sandwich', 0.0], ['pie', 12.603], ['muffin', 14.301],
                                 ['cookie', 12.219], ['frappucino', 17.15], ['soda', 15.51], ['coffee', 16.6],
                                 ['tea', 15.78], ['water', 17.86], ['milkshake', 17.1]],
                    '17:28:00': [['', 60.329], ['sandwich', 0.0], ['pie', 13.753], ['muffin', 13.041],
                                 ['cookie', 12.877], ['frappucino', 16.88], ['soda', 16.66], ['coffee', 17.81],
                                 ['tea', 16.93], ['water', 14.68], ['milkshake', 17.04]],
                    '17:32:00': [['', 60.986], ['sandwich', 0.0], ['pie', 13.26], ['muffin', 12.11], ['cookie', 13.644],
                                 ['frappucino', 16.05], ['soda', 17.97], ['coffee', 16.6], ['tea', 17.04],
                                 ['water', 16.05], ['milkshake', 16.27]],
                    '17:36:00': [['', 60.384], ['sandwich', 0.0], ['pie', 13.096], ['muffin', 13.753],
                                 ['cookie', 12.767], ['frappucino', 17.04], ['soda', 16.77], ['coffee', 15.73],
                                 ['tea', 16.99], ['water', 16.16], ['milkshake', 17.32]],
                    '17:40:00': [['', 58.466], ['sandwich', 0.0], ['pie', 13.753], ['muffin', 13.699],
                                 ['cookie', 14.082], ['frappucino', 17.97], ['soda', 15.84], ['coffee', 16.55],
                                 ['tea', 16.49], ['water', 16.05], ['milkshake', 17.1]],
                    '17:44:00': [['', 60.329], ['sandwich', 0.0], ['pie', 12.822], ['muffin', 14.356],
                                 ['cookie', 12.493], ['frappucino', 16.82], ['soda', 17.42], ['coffee', 14.79],
                                 ['tea', 16.77], ['water', 17.26], ['milkshake', 16.93]],
                    '17:48:00': [['', 58.575], ['sandwich', 0.0], ['pie', 13.808], ['muffin', 14.137],
                                 ['cookie', 13.479], ['frappucino', 16.55], ['soda', 16.27], ['coffee', 17.97],
                                 ['tea', 16.05], ['water', 16.33], ['milkshake', 16.82]],
                    '17:52:00': [['', 59.342], ['sandwich', 0.0], ['pie', 13.973], ['muffin', 13.151],
                                 ['cookie', 13.534], ['frappucino', 16.44], ['soda', 16.33], ['coffee', 18.36],
                                 ['tea', 17.48], ['water', 15.18], ['milkshake', 16.22]],
                    '17:56:00': [['', 59.342], ['sandwich', 0.0], ['pie', 11.726], ['muffin', 14.247],
                                 ['cookie', 14.685], ['frappucino', 18.19], ['soda', 17.59], ['coffee', 15.62],
                                 ['tea', 15.62], ['water', 15.62], ['milkshake', 17.37]]}
    timelist = ['08:00:00', '08:05:00', '08:10:00', '08:15:00', '08:20:00', '08:25:00', '08:30:00', '08:35:00',
                '08:40:00', '08:45:00', '08:50:00', '08:55:00', '09:00:00', '09:05:00', '09:10:00', '09:15:00',
                '09:20:00', '09:25:00', '09:30:00', '09:35:00', '09:40:00', '09:45:00', '09:50:00', '09:55:00',
                '10:00:00', '10:05:00', '10:10:00', '10:15:00', '10:20:00', '10:25:00', '10:30:00', '10:35:00',
                '10:40:00', '10:45:00', '10:50:00', '10:55:00', '11:00:00', '11:02:00', '11:04:00', '11:06:00',
                '11:08:00', '11:10:00', '11:12:00', '11:14:00', '11:16:00', '11:18:00', '11:20:00', '11:22:00',
                '11:24:00', '11:26:00', '11:28:00', '11:30:00', '11:32:00', '11:34:00', '11:36:00', '11:38:00',
                '11:40:00', '11:42:00', '11:44:00', '11:46:00', '11:48:00', '11:50:00', '11:52:00', '11:54:00',
                '11:56:00', '11:58:00', '12:00:00', '12:02:00', '12:04:00', '12:06:00', '12:08:00', '12:10:00',
                '12:12:00', '12:14:00', '12:16:00', '12:18:00', '12:20:00', '12:22:00', '12:24:00', '12:26:00',
                '12:28:00', '12:30:00', '12:32:00', '12:34:00', '12:36:00', '12:38:00', '12:40:00', '12:42:00',
                '12:44:00', '12:46:00', '12:48:00', '12:50:00', '12:52:00', '12:54:00', '12:56:00', '12:58:00',
                '13:00:00', '13:04:00', '13:08:00', '13:12:00', '13:16:00', '13:20:00', '13:24:00', '13:28:00',
                '13:32:00', '13:36:00', '13:40:00', '13:44:00', '13:48:00', '13:52:00', '13:56:00', '14:00:00',
                '14:04:00', '14:08:00', '14:12:00', '14:16:00', '14:20:00', '14:24:00', '14:28:00', '14:32:00',
                '14:36:00', '14:40:00', '14:44:00', '14:48:00', '14:52:00', '14:56:00', '15:00:00', '15:04:00',
                '15:08:00', '15:12:00', '15:16:00', '15:20:00', '15:24:00', '15:28:00', '15:32:00', '15:36:00',
                '15:40:00', '15:44:00', '15:48:00', '15:52:00', '15:56:00', '16:00:00', '16:04:00', '16:08:00',
                '16:12:00', '16:16:00', '16:20:00', '16:24:00', '16:28:00', '16:32:00', '16:36:00', '16:40:00',
                '16:44:00', '16:48:00', '16:52:00', '16:56:00', '17:00:00', '17:04:00', '17:08:00', '17:12:00',
                '17:16:00', '17:20:00', '17:24:00', '17:28:00', '17:32:00', '17:36:00', '17:40:00', '17:44:00',
                '17:48:00', '17:52:00', '17:56:00']
    pricefooddictionary = {"": 0, "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3}
    pricedrinkdictionary = {"frappucino": 4, "soda": 3, "tea": 3, "water": 2, "milkshake": 5, "coffee": 3}
    inclist = []
    regularcustomer = []
    customer = {}
    custtype = ["RETURNING", "ONETIME"]
    onetimecust = ["TRIPADVISOR", "REGULAR"]

    # Create the id of the new customers

    def idcreator(regular, hipster):

        # CID12345678
        current = "CID"
        nbnewreg = 0
        nbnewhip = 0
        listidcreated = []
        id = 10000000

        while nbnewreg < regular:
            id += 1
            idcreated = current + str(id)
            listidcreated.append(idcreated)
            nbnewreg += 1

        while nbnewhip < hipster:
            id += 1
            idcreated = current + str(id)
            listidcreated.append(idcreated)
            nbnewhip += 1
        return listidcreated

    # Create regular one time customers

    def createregularot(idcreated):
        hist = {"totalprice": 0, "food": [], "drink": []}
        info = [100, "ROT", hist]
        customer[idcreated] = info

    # Create tripadvisor one time customers

    def createtripadvisor(idcreated):
        hist = {"totalprice": 0, "food": [], "drink": []}
        info = [100, "TRIP", hist]
        customer[idcreated] = info

    # Create regular and hipster customers

    def create_regular(hipster, regular):
        nbnewhip = 0
        nbnewreg = 0
        nbreturning = 0
        listid = idcreator(regular, hipster)

        while nbnewhip < hipster:
            idcreated = listid[nbreturning]
            info = [500, "HIP", {"totalprice": 0, "food": [], "drink": []}]
            customer[idcreated] = info
            regularcustomer.append((idcreated, info))
            nbnewhip += 1
            nbreturning += 1

        while nbnewreg < regular:
            idcreated = listid[nbreturning]
            info = [250, "RRET", {"totalprice": 0, "food": [], "drink": []}]
            customer[idcreated] = info
            regularcustomer.append((idcreated, info))
            nbnewreg += 1
            nbreturning += 1

    # Assigne a drink following the probabilities

    def probadrink(hour):
        interlist = []
        problist = []
        position = 10
        sumprob = 0

        while position > 4:
            probdecimal = probapertime[hour][position][1] / 100
            sumprob += probdecimal
            interlist.append(probdecimal)
            position -= 1

        for prob in interlist:
            nbweighted = prob / sumprob
            problist.append(nbweighted)

        probdrink = np.random.choice(drinklist, 1, p=problist)

        # Arrange probdrink to fit the right form in the general function

        probdrink2 = np.array_str(probdrink)
        probdrink3 = probdrink2[2:-2]

        totalstock = 0
        for drink, stock in nbInStockDrink.items():
            totalstock += stock
        if totalstock == 0 :
            return 'no more drinks available'


        if nbInStockDrink[probdrink3] < 1:
            print(probdrink3 + ' are out of stock')
            probadrink(hour)
        else :
            nbInStockDrink[probdrink3] -= 1

        return probdrink3

    # Assigne a food or not following the probabilities

    def probafood(hour):
        interlist = []
        problist = []
        position = 0
        sumprob = 0

        while position < 5:
            probdecimal = probapertime[hour][position][1] / 100
            sumprob += probdecimal
            interlist.append(probdecimal)
            position += 1

        for prob in interlist:
            nbweighted = prob / sumprob
            problist.append(nbweighted)

        probfood = np.random.choice(foodlist, 1, p=problist)

        # Arrange probfood to fit the right form in the general function

        probfood2 = np.array_str(probfood)
        probfood3 = probfood2[2:-2]

        totalstock = 0
        for food, stock in nbInStockFood.items():
            totalstock += stock
        if totalstock == 0:
            return 'no more food available'
        if probfood3 != '':
            if nbInStockFood[probfood3] < 1:
                print(probfood3 + ' are out of stock!')
                probafood(hour)
            else:
                nbInStockFood[probfood3] -= 1
        return probfood3

    def enoughmoney(customerid):

        if customer[customerid][0] < 10:
            return False
        else:
            return True

    # Credit the customer, add food and drink in customer's dictionnaries and compute the total sum of money spent since
    # the beginning

    def purchase(customerid, drink, food):
        if food != "":
            (customer[customerid][2]["food"]).append(food)
            pricefood = pricefooddictionary[food]

        else:
            pricefood = 0

        listdrink = customer[customerid][2]["drink"]
        listdrink.append(drink)
        pricedrink = pricedrinkdictionary[drink]
        expense = pricedrink + pricefood
        if customer[customerid][1] == "TRIP":
            tip = random.randint(1, 10)
            expense += tip

        customer[customerid][0] -= expense
        customer[customerid][2]["totalprice"] += expense
        return expense

    # Create regular customers

    create_regular(hipster, regular)

    #  Beginning of the simulation

    while itera-1 < days:
        dailyexp = 0
        numbid = 1
        for hour in timelist:
            idonetime = 300000 + itera*numbid

            # Choose customer
            probcust = np.random.choice(custtype, 1, p=[0.2, 0.8])
            if probcust == "RETURNING":
                if len(regularcustomer) > 0:
                    cust = random.choice(regularcustomer)
                else:
                    cust = np.random.choice(onetimecust, 1, p=[0.1, 0.9])
            else:
                cust = np.random.choice(onetimecust, 1, p=[0.1, 0.9])

            # Get the id of the customer
            if cust == "TRIPADVISOR":
                idcurrentcust = idonetime
                createtripadvisor(idcurrentcust)

            elif cust == "REGULAR":
                idcurrentcust = idonetime
                createregularot(idcurrentcust)

            else:
                idcurrentcust = cust[0]

            # Get a article of drink and food

            articleofdrink = probadrink(hour)
            articleoffood = probafood(hour)

            if articleofdrink == 'no more drink available':
                return
            if articleoffood == 'no more food available':
                return
            if articleoffood != '':
                numberf = newfooddict[articleoffood]
                numberf += 1
                newfooddict[articleoffood] = numberf

            # Compute the total number of food and drink of the simulation

            numberd = newdrinkdict[articleofdrink]
            numberd += 1
            newdrinkdict[articleofdrink] = numberd

            # Credit the customer, add food and drink in customer's dictionnaries and compute the total sum of money
            # spent since the beginning

            exp = purchase(idcurrentcust, articleofdrink, articleoffood)

            # Check if customer still have enough money for the next purchase

            money = enoughmoney(idcurrentcust)
            info = customer[idcurrentcust]

            if money is False and (idcurrentcust, info) in regularcustomer:
                regularcustomer.remove((idcurrentcust, info))
                print(idcurrentcust, info)

            dailyexp += exp
            suminc += exp
            numbid += 1
        itera += 1
        print(itera)

        inclist.append(dailyexp)

    # Graph of the income of each day during the period

    plt.hist(inclist, bins=100)
    plt.xlabel('Income')
    plt.ylabel(u'Occurence')
    plt.axis([800, 1150, 0, 70])
    plt.grid(True)
    plt.title("Income per day")
    plt.savefig("IncomePerdayGraph.png")
    plt.show()

    # Graph of the number of drinks sold

    plt.bar(list(newdrinkdict.keys()), newdrinkdict.values(), color='y')
    plt.title("Amount of drinks sold")
    plt.ylabel("Number of drinks sold")
    plt.xlabel("Kind of drinks")
    plt.savefig("DrinksSoldSimulation.png")
    plt.show()

    # Graph of the number of foods sold

    plt.bar(list(newfooddict.keys()), newfooddict.values(), color='b')
    plt.title("Amount of food sold")
    plt.ylabel("Number of food sold")
    plt.xlabel("Kind of food")
    plt.savefig("FoodsSoldSimulation.png")
    plt.show()


    return customer


general_function(1825, 666, 333)

